//
//  secondViewController.swift
//  test
//
//  Created by enas on 7/20/18.
//  Copyright © 2018 socyle. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class secondViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var productName: UILabel!
    
    
    @IBOutlet weak var ProductVCcell: UICollectionView!
    var singleItem:coffeItem?
    var itemId : Int!
    var Pname : String!
     var Productlist = Array<coffeItem>()
    var cart = [coffeItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

      self.navigationItem.title = "some title"

        ProductVCcell.delegate = self
        ProductVCcell.dataSource = self
        print("itemId---->",itemId)
        print("Pname---->",Pname)
       productName.text = Pname
       Loadproduct()
    }
    
    @objc func saveButtonWasPressed(_ sender: UIButton) {
        let product = Productlist[sender.tag]
       
        print(product.Name)
        if let i = cart.index(where: {$0.Name == product.Name}) {
          cart[i].quantity = cart[i].quantity! + 1
            print(cart[i].quantity!)
            
        } else {
            product.quantity = product.quantity! + 1
            cart.append(product)
            print(product.quantity!)
            for i in cart{
                
                let NewProducts = Products(context:context)
                NewProducts.pname = i.Name
                NewProducts.pprice = i.price
                NewProducts.pimage = i.image! as NSObject
                NewProducts.quantity = i.quantity!
                do{
                    
                    add.saveContext()
                    print("saved")
                    
                }catch{
                    
                    print("error")
                    
                }
               
            }
            
            
        }
        
        // Save Cart in Core Data
//        self.cart
        
    }

    func collectionView(_ collectionView: UICollectionView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 176.0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
            return 1
    
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
            return Productlist.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       
            let cell:TypeOfCoffe = collectionView.dequeueReusableCell(withReuseIdentifier: "Tcell", for: indexPath) as! TypeOfCoffe
        cell.PNmae.text = Productlist[indexPath.row].Name!
        cell.lblprice.text = Productlist[indexPath.row].price!
        cell.setImage3(url: Productlist[indexPath.row].image!)
        cell.saveButton.tag = indexPath.row
        cell.saveButton.addTarget(self, action: #selector(saveButtonWasPressed(_:)), for: .touchUpInside)
            return cell
            
    }
   

    
    func Loadproduct(){
    
        let first = main_URL + baseUrl + "products?category="
        let second = String(itemId) + "&" + last_part(time: String (time()), s:finalString())
        let  FinalUrl2 = first + second
        
        
        Alamofire.request(FinalUrl2, method: .get)
            .responseData { response in
                
                switch response.result
                {
                case .failure(let errer):
                    print(errer)
                case .success(let value):
                    print(value)
                    do {
                        if let json = try JSONSerialization.jsonObject(with: value) as? [[String: Any]]{
                            
                            
                            
                            var PName = ""
                            var Pprice = ""
                            var Pimag = ""
                            let Pid = 0
                            for data in json  {
                                
                               
                                if let images = data["images"] as? [[String:Any]]{
                                    
                                    for Pimg in images{
                                      
                                        if let Pimages = Pimg["src"] as? String{
                                           Pimag = Pimages
                                            print ("src", Pimag)
                                        }
                                      
                                    }//end for
                                    
                                }//end images
                                
                                
                                
                                if let Nname = data["name"] as? String {
                                    
                                    PName = Nname
                                    print("name => ",Nname)
                                }
                                
                                if   let price = data["price"] as? String {
                                    Pprice = price
                                    print("price => ",price)
                                  }
                                self.Productlist.append(coffeItem(Name:PName, image:Pimag , price:Pprice, id: Pid, quantity: 0))
                        
                            } // end for
                        self.ProductVCcell.reloadData()
                            
                            
                        }//end json
                    } catch {
                        print("Error deserializing JSON: \(error)")
                    }
                    
                    
                }//end swith
                
        }//end responseData
        
        
        
    }//end product
    
    
  
    
    
    
    
    
    
    
    
    
    
    
}
