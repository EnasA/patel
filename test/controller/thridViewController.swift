//
//  thridViewController.swift
//  test
//
//  Created by enas on 7/24/18.
//  Copyright © 2018 socyle. All rights reserved.
//

import UIKit
import CoreData

class thridViewController: UIViewController ,UITableViewDelegate , UITableViewDataSource{
    
    
    @IBOutlet weak var totalprice: UILabel!
    @IBOutlet weak var tvListProducts: UITableView!
    
   var listProducts = [Products]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

         LoadProduct()
    
        tvListProducts.delegate = self
        tvListProducts.dataSource = self
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        
        
        return listProducts.count
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RecycleTableViewCell = tableView.dequeueReusableCell(withIdentifier:"Rcell", for: indexPath) as! RecycleTableViewCell
        
        cell.SetCell(product: listProducts[indexPath.row])
        
        
      cell.deletedata.tag = indexPath.row
       cell.deletedata.addTarget(self, action: #selector(DeleteData(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    
    func LoadProduct(){
        
        let fetchRequest:NSFetchRequest<Products>=Products.fetchRequest()

        do{
            
            listProducts = try context.fetch(fetchRequest)
            tvListProducts.reloadData()
            
        }catch{
            
            print("error in reolad")
        }
        
        var TotlePrice:Double = 0.0
        for i in listProducts{
            TotlePrice += Double(i.pprice!)! * Double(i.quantity)
          
            self.totalprice.text = String(TotlePrice)
        }
        
        
    }
    
    @objc func DeleteData(sender : UIButton!) {

        do{
   
        if self.listProducts.count > 0 {
           let objectindex = self.listProducts.remove(at:sender.tag)
               context.delete(objectindex)
            for i in listProducts {
                i.pprice?.removeAll()
            }

            }
       try  context.save()
        }catch{}

        tvListProducts.reloadData()

      

    }
    
    @IBAction func btnBack(_ sender: AnyObject) {
        
        dismiss(animated: true, completion: nil)
        
        
    }
    
    
}
