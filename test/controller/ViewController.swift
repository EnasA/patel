//
//  ViewController.swift
//  test
//
//  Created by enas on 7/14/18.
//  Copyright © 2018 socyle. All rights reserved.
//

import UIKit
import CommonCryptoModule
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource {
   
   // @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionViewList: UICollectionView!
    @IBOutlet weak var secondCVcell: UICollectionView!
  
    @IBOutlet weak var SliderCVCell: UICollectionView!
    var imageSlider2 = [String]()
    let url = "http://www.batelalkhair.com/webservice.php"
    var scrollingTimer = Timer()
    
    var coffelist = Array<coffeItem>()
    var images = [String]()
    

    override func viewDidLoad() {
            super.viewDidLoad()
        
        
            collectionViewList.dataSource = self
            collectionViewList.delegate = self
            secondCVcell.delegate = self
            secondCVcell.dataSource = self
            SliderCVCell.delegate = self
            SliderCVCell.dataSource = self
        

        LoadData()
        
        
        loadImages()
       loadImagesSlider()
        
        }

    
 /*   @objc internal func updateTimer(){
        
        if(UpdateCount<=2){
            
            pageControl.currentPage = UpdateCount
            SliderImage.image = UIImage(named:String(UpdateCount+1) + ".png")
                UpdateCount = UpdateCount+1
        }else{
            
            UpdateCount = 0 
        }
            
        }
    */
    

///////////////////////////////CollectionView////////////////////////////////
    
    
    func collectionView(_ collectionView: UICollectionView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 176.0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if collectionView == self.collectionViewList {
            
              return 1
            
        }
        
        if collectionView == self.SliderCVCell{
            
            return 1
       
        }else{
        
              return 1
        }
        
    
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collectionViewList {
           
               return coffelist.count
        }
       
        if collectionView == self.SliderCVCell{
            
              return imageSlider2.count
        }
        else{
            
               return images.count
            
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
    if collectionView == self.collectionViewList{
        
        let cell:CoffeVCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Ccell", for: indexPath) as! CoffeVCell
        
        cell.laName.text = coffelist[indexPath.row].Name!
        cell.setImage(url: coffelist[indexPath.row].image!)
        
        return cell
        
    }
    
    if collectionView == self.SliderCVCell{
            
        
        let imgCell:SliderCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCell", for: indexPath) as! SliderCollectionViewCell
        
        // imgCell.myimage.image = UIImage(named:imageSlider[indexPath.row])
        imgCell.setImage3(url: imageSlider2[indexPath.row])
        
        var rowIndex = indexPath.row
        let numberOfRecords:Int = self.imageSlider2.count-1
        if(rowIndex<numberOfRecords){
            
            rowIndex = (rowIndex+1)
        }else{
            
            rowIndex = 0
        }
        
        
        scrollingTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(ViewController.startTimer(theTimer:)), userInfo: rowIndex, repeats: true)
        
        return imgCell
        
        
    }else{
        
        let cell2:giftsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GCell", for: indexPath) as! giftsCollectionViewCell
        
        
        cell2.setImage2(url: images[indexPath.row])
        
        return cell2
        
        
            }//end else
        
        }
    
    
    @objc func startTimer(theTimer:Timer){
        
        UIView.animate(withDuration: 2, delay: 0, options: .curveEaseOut, animations: {
            self.SliderCVCell.scrollToItem(at: IndexPath(row:theTimer.userInfo as! Int , section:0), at:.centeredHorizontally, animated: false)
            
        }, completion: nil)    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionViewList{
            
            performSegue(withIdentifier: "ShowDetails", sender: coffelist[indexPath.row].id)
            
            performSegue(withIdentifier: "ShowDetails", sender: coffelist[indexPath.row].Name)
            
        }else{
            
            print("User Tapped: \(indexPath.row)")
        }

    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let  dis = segue.destination as? secondViewController{
            if let indexPath = self.collectionViewList.indexPathsForVisibleItems.last{
                
                dis.itemId = coffelist[indexPath.row].id
               dis.Pname = coffelist[indexPath.row].Name
                
            }
            
        }
        
    }
    
    
///////////////////////////////LOAD Data///////////////////////////////////////
    
    func LoadData(){
        
        let FURL1 = URLS.FinalUrl
             print("FURL1===>", FURL1)
           
            Alamofire.request(FURL1, method: .get)
                .responseData { response in
                    
                    switch response.result
                    {
                    case .failure(let errer):
                        print(errer)
                    case .success(let value):
                        print(value)
                        do {
                            if let json = try JSONSerialization.jsonObject(with: value) as? [[String: Any]]{
                                
                                var RName = ""
                                var Rprice = ""
                                var Rimag = ""
                                var Rid = 0

                               
                                for data in json  {

                                  
                                    if let id  = (data["id"] as? Int){
                                        print("id->",id)
                                        
                                        Rid = id
                                        print("id->",id)
                                        
                                    }
                                 
                                    if let Nname = data["name"] as? String {
                                        
                                        RName = Nname
                                        print("name => ",Nname)
                                    }
                                    
                                    if   let price = data["description"] as? String {
                                        Rprice = price
                                        print("price => ",price)
                                    }
                                    
                                    if let img2 = data["image"] as? [String:Any] {
                                        print("img2 => ",img2)
                                        if let Fimage = img2["src"] as? String {
                                            Rimag = Fimage
                                            print(Fimage)
                                        }//end if
                                        
                                    }//end if img2
                                    
                                    
                                    self.coffelist.append(coffeItem(Name:RName, image:Rimag , price:Rprice, id: Rid, quantity: 0))


                                } // end for
                                self.collectionViewList.reloadData()
                               

                            }//end json
                        } catch {
                            print("Error deserializing JSON: \(error)")
                        }

                        
                    }//end swith
                    
            }//end responseData
            
            
            
        }//end loaddate
        
    
    
    
//////////////////////////////Load Image ///////////////////////////////////////
    
    func loadImages() {
        
        let parameters = [
            
            "get_gift_cat":true
            
            
        ]
        
        let url = URLS.urlPOST
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .responseData { response in
                
                switch response.result
                {
                case .failure(let errer):
                    print(errer)
                case .success(let value):
                    print(value)
                    // var images = [String]()
                    
                    do {
                        if let json = try JSONSerialization.jsonObject(with: value) as? [[String: Any]]{
                            for img in json  {
                                  if let image = img["image"] as? String {
                                    self.images.append(image)
                                    print("jjjj => ",image)
                                }//end if
                                
                            }//end for
                            
                            self.secondCVcell.reloadData()
                            
                        }// end if json
                    } catch {
                        print("Error deserializing JSON: \(error)")
                    }//catch
                
                }// end swith
        }// end response

}// end load image
    
    
    func loadImagesSlider() {
        
        let parameters = [
            
            "get_slider":true
            
            
        ]
        
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .responseData { response in
                
                switch response.result
                {
                case .failure(let errer):
                    print(errer)
                case .success(let value):
                    print(value)
                    // var images = [String]()
                    
                    do {
                        if let json = try JSONSerialization.jsonObject(with: value) as? [String:Any] {
                            
                            print(json)
                            
                            
                            if let one1 = json["1"] as? [String :Any] {
                                
                                // print("one" , one1)
                                if  let img = one1["image"] as? String {
                                    print (img)
                                    self.imageSlider2.append(img)
                                    
                                }
                                
                            }
                            
                            if let two2 = json["2"] as? [String :Any] {
                                
                                // print("two2" , two2)
                                if  let img2 = two2["image"] as? String {
                                    print (img2)
                                    self.imageSlider2.append(img2)
                                }
                                
                            }
                            
                            if let three3 = json["3"] as? [String :Any] {
                                
                                //  print("three3" , three3)
                                if  let img3 = three3["image"] as? String {
                                    print (img3)
                                    self.imageSlider2.append(img3)
                                }
                                
                            }
                            
                            if let four4 = json["4"] as? [String :Any] {
                                
                                // print("four4" , four4)
                                if  let img4 = four4["image"] as? String {
                                    print (img4)
                                    self.imageSlider2.append(img4)
                                }
                                
                            }
                            
                            self.SliderCVCell.reloadData()
                            
                        }
                    } catch {
                        print("Error deserializing JSON: \(error)")
                    }
                    
                    
                }
                
        }
        
        
    }
    
    
    
    
    
    
    
    
    
}
