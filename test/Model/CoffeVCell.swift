//
//  CoffeVCell.swift
//  ImageSlideshow_Example
//
//  Created by enas on 7/13/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit

class CoffeVCell: UICollectionViewCell {
    

    @IBOutlet weak var myimage: UIImageView!
    
    @IBOutlet weak var laName: UILabel!
    
    func setImage(url:String)  {
        let urlString =  url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        DispatchQueue.global().async {
            
            let Aurl = URL(string: urlString!)
            let data = NSData(contentsOf: Aurl!)
            
            if data != nil {
                self.myimage.image = UIImage(data:data! as Data)
                
                
            }
            
            
        }
        
        
        
    }
    
    
}

    

