//
//  SliderCollectionViewCell.swift
//  SLider2
//
//  Created by enas on 7/22/18.
//  Copyright © 2018 socyle. All rights reserved.
//

import UIKit
import Kingfisher

class SliderCollectionViewCell: UICollectionViewCell {
  
    @IBOutlet weak var myimage: UIImageView!
    
    func setImage3(url: String) {
        guard let url = URL(string: url) else { return }
        myimage.kf.setImage(with: url)
    }
//    func setImage3(url:String)  {
//        let urlString =  url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//        DispatchQueue.global().async {
//            
//            let Aurl = URL(string: urlString!)
//            let data = NSData(contentsOf: Aurl!)
//            
//            if data != nil {
//                self.myimage.image = UIImage(data:data! as Data)
//                
//                
//            }
//            
//            
//        }
//
//        
//    }
    
    
    
    
    
}
