//
//  RecycleTableViewCell.swift
//  test
//
//  Created by enas on 7/24/18.
//  Copyright © 2018 socyle. All rights reserved.
//

import UIKit

class RecycleTableViewCell: UITableViewCell {


    @IBOutlet weak var Reimage: UIImageView!
    
    @IBOutlet weak var txtname: UILabel!
    
    @IBOutlet weak var txtprice: UILabel!
    
    @IBOutlet weak var txtQuantity: UITextField!
    
    
   @IBOutlet weak var deletedata: UIButton!
    
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func SetCell(product:Products) {
        txtname.text = product.pname
        txtprice.text = product.pprice
        Reimage.image = product.pimage as? UIImage
        txtQuantity.text = String(describing: product.quantity)
        
    }
}
