//
//  giftsCollectionViewCell.swift
//  ImageSlideshow_Example
//
//  Created by enas on 7/14/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import Kingfisher

class giftsCollectionViewCell: UICollectionViewCell {
    
   
    @IBOutlet weak var myimg: UIImageView!
    
    func setImage2(url: String) {
        guard let url = URL(string: url) else { return }
        myimg.kf.setImage(with: url)
    }
    
//    func setImage2(url:String)  {
//        let urlString =  url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//        DispatchQueue.global().async {
//
//            let Aurl = URL(string: urlString!)
//            let data = NSData(contentsOf: Aurl!)
//
//            if data != nil {
//                self.myimg.image = UIImage(data:data! as Data)
//
//
//            }
//
//
//        }
//    }
    
    
}

